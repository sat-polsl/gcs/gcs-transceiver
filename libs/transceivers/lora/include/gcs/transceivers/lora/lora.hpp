#pragma once
#include "gcs/transceivers/transceiver_base.hpp"
#include "gcs/transceivers/lora/configuration.hpp"
#include "gcs/serial/serial.hpp"
#include "gcs/devices/rn2483/rn2483.hpp"
#include "gcs/devices/rn2483/formatters.hpp"
#include <boost/asio.hpp>
#include <stdexcept>
#include <memory>
#include "azmq/socket.hpp"
#include "serialization/serialization_base.hpp"

#include "nlohmann/json.hpp"

#include "satext/struct.h"

namespace gcs::transceivers::lora {

class lora : public transceiver_base {
public:
    class radio_loop : public std::enable_shared_from_this<radio_loop> {
    public:
        explicit radio_loop(lora& l) : _lora{l} {}

        void start() {
            spdlog::debug("[LoRa.radio_loop] Starting loop");
            receive(std::chrono::seconds(70));
        }

        void operator()(boost::system::error_code error, devices::rn2483::status status,
                        std::vector<std::byte> data) {
            using namespace satext::struct_literals;
            spdlog::debug("[LoRa.radio_loop] Error: {}, Status: {}", error.message(), status);
            spdlog::debug("[LoRa.radio_loop] Received: {:#04x}", fmt::join(data, ", "));

            nlohmann::json deserialized;
            deserialized["timestamp"] = std::chrono::duration_cast<std::chrono::seconds>(
                                            std::chrono::system_clock::now().time_since_epoch())
                                            .count();
            if (status == devices::rn2483::status::radio_rx) {
                _lora._rx_protocol->deserialize(data, deserialized);
            } else {
                deserialized["radio_status"] = fmt::format("{}", status);
            }

            auto topic = std::make_unique<std::string>("beacon_rx");
            auto msg = std::make_unique<std::string>(deserialized.dump());

            spdlog::info("[LoRa.radio_loop] JSON:\n {}", deserialized.dump(4));
            std::array<boost::asio::const_buffer, 2> in = {
                boost::asio::const_buffer(topic->data(), topic->size()),
                boost::asio::const_buffer(msg->data(), msg->size()),
            };

            _lora._pub_socket.async_send(
                in, [topic = std::move(topic), msg = std::move(msg)](auto error, auto size) {
                    spdlog::debug("[LoRa.radio_loop.pub_socket] Topic: {}. Msg: {}.", *topic, *msg);
                    spdlog::debug("[LoRa.radio_loop.pub_socket] Error: {}. Sent {} bytes.",
                                  error.message(), size);
                });
            receive(std::chrono::seconds(70));
        }

    private:
        void receive(std::chrono::seconds timeout) {
            spdlog::debug("[LoRa.radio_loop] Receiving for {}s", timeout.count());
            _lora._radio.receive(timeout, this->shared_from_this());
        }

        lora& _lora;
    };

    lora(boost::asio::io_context& ctx, configuration cfg, azmq::pub_socket& pub_socket,
         std::shared_ptr<serialization::serialization_base> rx_protocol,
         std::shared_ptr<serialization::serialization_base> tx_protocol) :
        _serial{ctx},
        _radio{_serial},
        _configuration{cfg},
        _pub_socket{pub_socket},
        _rx_protocol{std::move(rx_protocol)},
        _tx_protocol{std::move(tx_protocol)} {}

    void start() override {
        _serial.open(_configuration.port);
        _serial.set_baudrate(_configuration.baudrate);

        configure_radio();
    }

private:
    void configure_radio() {
        _radio.configure(
            _configuration.radio_configuration,
            [this](boost::system::error_code error, devices::rn2483::configuration_status status) {
                if (error) {
                    spdlog::error("[LoRa.configure_radio] Error: {}", error.message());
                    throw std::runtime_error("LoRa configuration: " + error.message());
                }
                if (status == devices::rn2483::configuration_status::error) {
                    spdlog::error("[LoRa.configure_radio] Retrying configuration");
                    configure_radio();
                } else {
                    spdlog::debug("[LoRa.configure_radio] Radio configured");
                    std::make_shared<radio_loop>(*this)->start();
                }
            });
    }

    gcs::serial::serial _serial;
    gcs::devices::rn2483::rn2483 _radio;
    configuration _configuration;
    azmq::pub_socket& _pub_socket;
    std::shared_ptr<serialization::serialization_base> _rx_protocol;
    std::shared_ptr<serialization::serialization_base> _tx_protocol;
};
} // namespace gcs::transceivers::lora
