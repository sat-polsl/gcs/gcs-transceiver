#pragma once
#include <string>
#include "gcs/devices/rn2483/configuration.hpp"

namespace gcs::transceivers::lora {
struct configuration {
    std::string port;
    std::uint32_t baudrate;
    devices::rn2483::configuration radio_configuration;
};
} // namespace gcs::transceivers::lora
