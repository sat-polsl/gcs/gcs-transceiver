#include "gcs/transceivers/factory.hpp"
#include "gcs/transceivers/lora/lora.hpp"
#include "satext/overload.h"
#include "spdlog/spdlog.h"
#include <variant>

namespace gcs::transceivers::factory {

std::shared_ptr<gcs::transceivers::transceiver_base>
create(gcs::transceivers::lora::configuration config, boost::asio::io_context& ctx,
       azmq::pub_socket& pub_socket, std::shared_ptr<serialization::serialization_base> rx_protocol,
       std::shared_ptr<serialization::serialization_base> tx_protocol) {
    spdlog::debug("[transceivers.factory] Creating LoRa");
    return std::make_shared<gcs::transceivers::lora::lora>(ctx, config, pub_socket, rx_protocol,
                                                           tx_protocol);
}

void create(gcs::configuration::configuration config, boost::asio::io_context& ctx,
            azmq::pub_socket& pub_socket,
            std::shared_ptr<serialization::serialization_base> rx_protocol,
            std::shared_ptr<serialization::serialization_base> tx_protocol,
            std::vector<std::shared_ptr<gcs::transceivers::transceiver_base>>& output) {
    for (auto&& cfg : config.transceivers) {
        std::visit(
            satext::overload([&output, &ctx, &pub_socket, rx_protocol, tx_protocol](auto& cfg) {
                output.push_back(create(cfg, ctx, pub_socket, rx_protocol, tx_protocol));
            }),
            cfg);
    }
}
} // namespace gcs::transceivers::factory
