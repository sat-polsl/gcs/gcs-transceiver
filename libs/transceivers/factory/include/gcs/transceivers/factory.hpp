#pragma once
#include "gcs/configuration/types.hpp"
#include <memory>
#include "gcs/transceivers/transceiver_base.hpp"
#include "serialization/serialization_base.hpp"
#include <boost/asio.hpp>
#include "azmq/socket.hpp"

namespace gcs::transceivers::factory {

void create(gcs::configuration::configuration config, boost::asio::io_context& ctx,
            azmq::pub_socket& pub_socket,
            std::shared_ptr<serialization::serialization_base> rx_protocol,
            std::shared_ptr<serialization::serialization_base> tx_protocol,
            std::vector<std::shared_ptr<gcs::transceivers::transceiver_base>>& output);

}
