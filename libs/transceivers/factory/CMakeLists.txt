set(TARGET transceivers_factory)

add_library(${TARGET})

target_include_directories(${TARGET} PUBLIC include)

target_sources(${TARGET} PRIVATE
    src/factory.cpp
    )

target_link_libraries(${TARGET}
    PUBLIC
    configuration
    lora_transceiver
    transceiver_base
    serialization_base
    satext::satext
    Azmq::azmq
    spdlog
    )

