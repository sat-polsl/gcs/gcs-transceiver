set(TARGET transceiver_base)

add_library(${TARGET} INTERFACE)

target_include_directories(${TARGET} INTERFACE include)

target_sources(${TARGET} PRIVATE
    include/gcs/transceivers/transceiver_base.hpp
    )
