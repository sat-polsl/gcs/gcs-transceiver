#pragma once

namespace gcs::transceivers {
class transceiver_base {
public:
    virtual void start() = 0;

protected:
    ~transceiver_base() = default;
};
} // namespace gcs::transceivers