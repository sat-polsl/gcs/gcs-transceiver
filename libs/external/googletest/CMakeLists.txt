include(FetchContent)

FetchContent_Declare(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG main
)

FetchContent_MakeAvailable(googletest)

set_target_properties(gtest gmock gmock_main
    PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON
    )

set_target_properties(gtest_main
    PROPERTIES
    EXCLUDE_FROM_ALL True
    )

target_compile_options(gtest
    PRIVATE
    -Wno-psabi
    )
