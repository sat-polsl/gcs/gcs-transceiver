include(FetchContent)

FetchContent_Declare(
    satext
    GIT_REPOSITORY https://gitlab.com/sat-polsl/software/satext.git
    GIT_TAG v2.2.2
)

FetchContent_MakeAvailable(satext)
