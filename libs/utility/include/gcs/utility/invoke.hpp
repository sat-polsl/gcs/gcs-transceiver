#pragma once
#include "gcs/utility/type_traits.hpp"

namespace gcs::utility {
template<typename F, typename... Args>
auto invoke(F&& f, Args&&... args) {
    if constexpr (is_shared_ptr<std::remove_cvref_t<F>>::value) {
        return std::invoke(*std::forward<F>(f), std::forward<Args>(args)...);
    } else {
        return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    }
}
} // namespace gcs::utility
