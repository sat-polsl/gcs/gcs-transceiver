#pragma once
#include <string>
#include "gcs/configuration/types.hpp"

namespace gcs::configuration {

configuration read_configuration(std::string file);

}
