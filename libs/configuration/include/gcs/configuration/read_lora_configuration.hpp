#pragma once
#include <string>
#include "gcs/configuration/types.hpp"
#include "yaml-cpp/yaml.h"

namespace gcs::configuration {

transceiver_configuration read_lora_configuration(YAML::Node config);

}
