#pragma once
#include <string>
#include "gcs/configuration/types.hpp"
#include "yaml-cpp/yaml.h"

namespace gcs::configuration {

serializer_configuration read_sat_protocol_configuration(YAML::Node config);

}
