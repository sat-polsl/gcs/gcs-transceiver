#pragma once
#include <variant>
#include <vector>
#include "gcs/transceivers/lora/configuration.hpp"
#include "serialization/sat_protocol/configuration.hpp"
#include "serialization/fundamental_types/configuration.hpp"

namespace gcs::configuration {

using transceiver_configuration = std::variant<gcs::transceivers::lora::configuration>;
using serializer_configuration = std::variant<serialization::sat_protocol::configuration>;

struct configuration {
    std::vector<transceiver_configuration> transceivers{};
    serializer_configuration rx_protocol{};
    serializer_configuration tx_protocol{};
};

} // namespace gcs::configuration
