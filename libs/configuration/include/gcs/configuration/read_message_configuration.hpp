#pragma once
#include <string>
#include "gcs/configuration/types.hpp"
#include "serialization/sat_protocol/configuration.hpp"
#include <vector>
#include "yaml-cpp/yaml.h"

namespace gcs::configuration {

std::vector<serialization::sat_protocol::message_configuration>
read_message_configuration(YAML::Node config);

}
