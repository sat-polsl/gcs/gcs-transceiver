#include "gcs/configuration/read_sat_protocol_configuration.hpp"
#include "serialization/sat_protocol/configuration.hpp"
#include "gcs/configuration/read_message_configuration.hpp"
#include "spdlog/spdlog.h"
#include <stdexcept>

namespace gcs::configuration {

gcs::configuration::serializer_configuration read_sat_protocol_configuration(YAML::Node config) {
    spdlog::debug("[read_sat_protocol_configuration] Config:\n{}", Dump(config));
    serialization::sat_protocol::configuration result{.station_id = 0x7fffffff};

    if (config["station_id"]) {
        result.station_id = config["station_id"].as<std::uint32_t>();
    } else {
        throw std::runtime_error("[read_sat_protocol_configuration] Missing 'station_id'");
    }

    config.remove("station_id");
    for (auto&& message : config) {
        auto message_id = message.first.as<std::uint8_t>();
        result.messages[serialization::sat_protocol::message_id{message_id}] =
            read_message_configuration(message.second);
    }

    return result;
}
} // namespace gcs::configuration
