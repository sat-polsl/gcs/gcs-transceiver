#include "gcs/configuration/read_configuration.hpp"
#include "gcs/configuration/read_lora_configuration.hpp"
#include "gcs/configuration/read_sat_protocol_configuration.hpp"
#include "yaml-cpp/yaml.h"
#include "spdlog/spdlog.h"
#include <map>
#include <functional>

namespace gcs::configuration {

using transceiver_configuration_reader = std::function<transceiver_configuration(YAML::Node)>;
using serializer_configuration_reader = std::function<serializer_configuration(YAML::Node)>;

configuration read_configuration(std::string file) {
    std::map<std::string_view, transceiver_configuration_reader> transceiver_configuration_map = {
        {"lora", read_lora_configuration}};

    std::map<std::string_view, serializer_configuration_reader> serializer_configuration_map = {
        {"sat_protocol", read_sat_protocol_configuration}};

    YAML::Node config = YAML::LoadFile(file);
    spdlog::debug("[read_configuration] Config:\n{}", Dump(config));

    configuration result;

    if (auto transceivers = config["transceivers"]; transceivers) {
        for (auto&& node : transceivers) {
            auto name = node.first.as<std::string>();
            if (transceiver_configuration_map.contains(name)) {
                result.transceivers.push_back(transceiver_configuration_map[name](node.second));
            }
        }
    }

    if (auto rx = config["rx"]; rx) {
        for (auto&& node : rx) {
            auto name = node.first.as<std::string>();
            if (serializer_configuration_map.contains(name)) {
                result.rx_protocol = serializer_configuration_map[name](node.second);
            }
        }
    }

    return result;
}

} // namespace gcs::configuration
