#include "gcs/configuration/read_lora_configuration.hpp"
#include "spdlog/spdlog.h"

namespace gcs::configuration {

std::string_view map_spreading_factor(std::string sf) {
    if (sf == devices::rn2483::spreading_factor::sf7) {
        return devices::rn2483::spreading_factor::sf7;
    } else if (sf == devices::rn2483::spreading_factor::sf8) {
        return devices::rn2483::spreading_factor::sf8;
    } else if (sf == devices::rn2483::spreading_factor::sf9) {
        return devices::rn2483::spreading_factor::sf9;
    } else if (sf == devices::rn2483::spreading_factor::sf10) {
        return devices::rn2483::spreading_factor::sf10;
    } else if (sf == devices::rn2483::spreading_factor::sf11) {
        return devices::rn2483::spreading_factor::sf11;
    } else if (sf == devices::rn2483::spreading_factor::sf12) {
        return devices::rn2483::spreading_factor::sf12;
    } else {
        throw std::runtime_error("[read_lora_configuration] Invalid spreading factor. Possible "
                                 "values: sf7, sf8, sf9, sf10, sf11, sf12");
    }
}

devices::rn2483::bandwidth map_bandwidth(std::uint32_t bandwidth) {
    if (bandwidth == 125) {
        return devices::rn2483::bandwidth::bw125khz;
    } else if (bandwidth == 250) {
        return devices::rn2483::bandwidth::bw250khz;
    } else if (bandwidth == 500) {
        return devices::rn2483::bandwidth::bw500khz;
    } else {
        throw std::runtime_error("[read_lora_configuration] Invalid bandwidth. Possible values: "
                                 "125, 250, 500");
    }
}

transceiver_configuration read_lora_configuration(YAML::Node config) {
    spdlog::debug("[read_lora_configuration] Config:\n{}", Dump(config));
    gcs::transceivers::lora::configuration result{};
    if (config["port"]) {
        result.port = config["port"].as<std::string>();
    }
    if (config["baudrate"]) {
        result.baudrate = config["baudrate"].as<std::uint32_t>();
    }
    if (config["watchdog_timeout_s"]) {
        result.radio_configuration.watchdog_timeout =
            std::chrono::seconds(config["watchdog_timeout_s"].as<std::uint32_t>());
    }
    if (config["frequency"]) {
        result.radio_configuration.frequency = config["frequency"].as<std::uint32_t>();
    }
    if (config["spreading_factor"]) {
        result.radio_configuration.spreading_factor =
            map_spreading_factor(config["spreading_factor"].as<std::string>());
    }
    if (config["bandwidth"]) {
        result.radio_configuration.lora_bandwidth =
            map_bandwidth(config["bandwidth"].as<std::uint32_t>());
    }

    spdlog::debug("[read_lora_configuration] Parsed config: {}, {}, {}, {}", result.port,
                  result.baudrate, result.radio_configuration.watchdog_timeout.count(),
                  result.radio_configuration.frequency);

    return result;
}

} // namespace gcs::configuration
