#include "gcs/configuration/read_message_configuration.hpp"
#include "spdlog/spdlog.h"
#include <stdexcept>

namespace gcs::configuration {

serialization::sat_protocol::message_configuration
read_telemetry_buffer_configuration(YAML::Node config) {
    serialization::telemetry_buffer::configuration result{};
    spdlog::debug("[read_telemetry_buffer_configuration] Config:\n{}", Dump(config));
    for (auto&& node : config) {
        auto id = node.begin()->first.as<std::uint32_t>();
        auto name = node.begin()->second.as<std::string>();
        spdlog::debug("[read_telemetry_buffer_configuration] Id: {}, Name: {}", id, name);
        result.keys.push_back({id, name});
    }
    return result;
}

serialization::sat_protocol::message_configuration read_message_seralizer(YAML::Node node) {
    spdlog::debug("[read_message_serializer] Config:\n{}", Dump(node));

    if (node.size() != 1) {
        throw std::runtime_error("[read_message_serializer] Wrong node size");
    }

    auto head = node.begin()->first.as<std::string>();
    auto tail = node.begin()->second;

    if (head == "telemetry_buffer") {
        return read_telemetry_buffer_configuration(tail);
    } else if (head == "u8") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = std::uint8_t{}};
    } else if (head == "u16") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = std::uint16_t{}};
    } else if (head == "u32") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = std::uint32_t{}};
    } else if (head == "i8") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = std::int8_t{}};
    } else if (head == "i16") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = std::int16_t{}};
    } else if (head == "i32") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = std::int32_t{}};
    } else if (head == "float") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = float{}};
    } else if (head == "bool") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = bool{}};
    } else if (head == "char") {
        return serialization::fundamental_types::configuration{.name = tail.as<std::string>(),
                                                               .type = char{}};
    } else {
        throw std::runtime_error("[read_message_serializer] Unrecognized type");
    }
}

std::vector<serialization::sat_protocol::message_configuration>
read_message_configuration(YAML::Node config) {
    std::vector<serialization::sat_protocol::message_configuration> result{};
    spdlog::debug("[read_message_configuration] Config:\n{}", Dump(config));

    for (auto&& node : config) {
        result.push_back(read_message_seralizer(node));
    }

    return result;
}
} // namespace gcs::configuration
