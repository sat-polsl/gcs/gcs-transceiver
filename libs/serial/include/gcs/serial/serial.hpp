#pragma once
#include <boost/asio.hpp>
#include <string_view>
#include <span>
#include "satext/noncopyable.h"

namespace gcs::serial {
class serial : private satext::noncopyable {
public:
    serial(boost::asio::io_context& ctx);

    void open(std::string_view port);
    void set_baudrate(std::uint32_t baudrate);

    template<typename Handler>
    void async_write(std::span<const std::byte> buffer, Handler&& handler) {
        _serial.async_write_some(boost::asio::buffer(buffer.data(), buffer.size()),
                                 std::forward<Handler>(handler));
    }

    template<typename Handler>
    void async_read(std::span<std::byte> buffer, Handler&& handler) {
        _serial.async_read_some(boost::asio::buffer(buffer.data(), buffer.size()),
                                std::forward<Handler>(handler));
    }

    void cancel();

private:
    boost::asio::serial_port _serial;
};

} // namespace gcs::serial