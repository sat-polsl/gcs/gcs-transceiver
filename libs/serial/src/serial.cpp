#include "gcs/serial/serial.hpp"
#include "spdlog/spdlog.h"

namespace gcs::serial {

serial::serial(boost::asio::io_context& ctx) : _serial{ctx} {}

void serial::open(std::string_view port) {
    spdlog::debug("[Serial] Opening port: {}", port);
    _serial.open(std::string(port));
}

void serial::set_baudrate(std::uint32_t baudrate) {
    spdlog::debug("[Serial] Setting baudrate: {}", baudrate);
    _serial.set_option(boost::asio::serial_port_base::baud_rate(baudrate));
}

void serial::cancel() { _serial.cancel(); }

} // namespace gcs::serial
