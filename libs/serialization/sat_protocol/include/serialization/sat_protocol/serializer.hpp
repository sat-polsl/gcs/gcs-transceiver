#pragma once
#include "serialization/serialization_base.hpp"
#include "serialization/sat_protocol/enums.hpp"
#include <map>
#include <memory>

namespace serialization::sat_protocol {

class sat_protocol : public serialization_base {
public:
    sat_protocol(std::uint8_t station_id);

    void serialize(const nlohmann::json& input, std::span<std::byte> output) override;

    void deserialize(std::span<const std::byte> input, nlohmann::json& output) override;

    void add_next(message_id id, std::shared_ptr<serialization_base> next);

private:
    static constexpr std::size_t sat_header_size = 10;

    std::uint8_t _station_id;

    std::map<message_id, std::shared_ptr<serialization_base>> _next;
};

} // namespace serialization::sat_protocol
