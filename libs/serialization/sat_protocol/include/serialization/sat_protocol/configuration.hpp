#pragma once
#include <cstdint>
#include <map>
#include <vector>
#include "serialization/fundamental_types/configuration.hpp"
#include "serialization/sat_protocol/enums.hpp"
#include "serialization/telemetry_buffer/configuration.hpp"

namespace serialization::sat_protocol {

using message_configuration = std::variant<serialization::fundamental_types::configuration,
                                           serialization::telemetry_buffer::configuration>;

struct configuration {
    std::uint32_t station_id;
    std::map<message_id, std::vector<message_configuration>> messages;
};

} // namespace serialization::sat_protocol
