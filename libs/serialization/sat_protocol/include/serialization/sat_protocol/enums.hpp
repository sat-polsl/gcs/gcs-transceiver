#pragma once
#include <cstdint>

namespace serialization::sat_protocol {
enum class message_id : std::uint8_t { beacon = 0 };
}
