#include "serialization/sat_protocol/serializer.hpp"
#include "satext/struct.h"
#include <stdexcept>
#include "spdlog/spdlog.h"

using namespace satext::struct_literals;

namespace serialization::sat_protocol {

sat_protocol::sat_protocol(std::uint8_t station_id) : _station_id{station_id} {}

void sat_protocol::serialize(const nlohmann::json&, std::span<std::byte>) {}

void sat_protocol::deserialize(std::span<const std::byte> input, nlohmann::json& output) {
    if (input.size() < sat_header_size) {
        throw std::runtime_error("[sat_protocol.deserialize] Buffer size too small");
    }

    spdlog::trace("[sat_protocol.deserialize] Input: {:#04x}", fmt::join(input, ","));
    satext::unpack("<6sBBBB"_fmt, input)
        .map([this, &output, input](auto result) {
            auto [callsign, id, tx_counter, msg_id, msg_size] = result;
            output["callsign"] = callsign;
            output["spacecraft_id"] = id;
            output["tx_counter"] = tx_counter;
            output["message_id"] = msg_id;
            output["message_size"] = msg_size;
            output["station_id"] = _station_id;

            spdlog::trace("[sat_protocol.deserialize] Deserialized: {}", output.dump());

            if (_next[message_id(msg_id)]) {
                spdlog::trace("[sat_protocol.deserialize] Calling next", fmt::join(input, ","));
                _next[message_id(msg_id)]->deserialize(input.subspan(sat_header_size), output);
            }
        })
        .map_error([](auto) {
            throw std::runtime_error("[sat_protocol.deserialize] Deserialization error");
        });
}

void sat_protocol::add_next(message_id id, std::shared_ptr<serialization_base> next) {
    _next[id] = next;
}

} // namespace serialization::sat_protocol
