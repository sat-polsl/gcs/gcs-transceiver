#pragma once
#include "nlohmann/json.hpp"
#include <span>

namespace serialization {
class serialization_base {
public:
    virtual void serialize(const nlohmann::json&, std::span<std::byte>) = 0;
    virtual void deserialize(std::span<const std::byte>, nlohmann::json&) = 0;

protected:
    ~serialization_base() = default;
};
} // namespace serialization
