#pragma once
#include <cstdint>
#include <string_view>
#include "serialization/fundamental_types/fundamental_type.hpp"

namespace serialization::fundamental_types {

template<fundamental_type T>
struct type_name {};

template<>
struct type_name<std::uint8_t> {
    static constexpr std::string_view name = "u8";
};

template<>
struct type_name<std::uint16_t> {
    static constexpr std::string_view name = "u16";
};

template<>
struct type_name<std::uint32_t> {
    static constexpr std::string_view name = "u32";
};

template<>
struct type_name<std::int8_t> {
    static constexpr std::string_view name = "i8";
};

template<>
struct type_name<std::int16_t> {
    static constexpr std::string_view name = "i16";
};

template<>
struct type_name<std::int32_t> {
    static constexpr std::string_view name = "i32";
};

template<>
struct type_name<float> {
    static constexpr std::string_view name = "float";
};

template<>
struct type_name<char> {
    static constexpr std::string_view name = "char";
};

template<>
struct type_name<bool> {
    static constexpr std::string_view name = "bool";
};

} // namespace serialization::fundamental_types
