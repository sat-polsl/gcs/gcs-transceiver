#pragma once
#include <string>
#include <variant>
#include <cstdint>

namespace serialization::fundamental_types {

struct configuration {
    using types_variant = std::variant<std::uint8_t, std::uint16_t, std::uint32_t, std::int8_t,
                                       std::int16_t, std::int32_t, float, char, bool>;

    std::string name;
    types_variant type;
};
} // namespace serialization::fundamental_types
