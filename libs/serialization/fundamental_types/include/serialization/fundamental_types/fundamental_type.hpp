#pragma once
#include <concepts>

namespace serialization::fundamental_types {

template<typename T>
concept fundamental_type = std::is_fundamental_v<T>;

}
