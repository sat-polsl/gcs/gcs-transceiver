#pragma once
#include <concepts>
#include "serialization/serialization_base.hpp"
#include "satext/struct.h"
#include <stdexcept>
#include <memory>
#include "spdlog/spdlog.h"
#include "serialization/fundamental_types/fundamental_type.hpp"
#include "serialization/fundamental_types/type_names.hpp"

namespace serialization::fundamental_types {

template<fundamental_type T>
class type : public serialization_base {
public:
    explicit type(std::string name) : _name{std::move(name)} {}

    void serialize(const nlohmann::json&, std::span<std::byte>) override {}

    void deserialize(std::span<const std::byte> input, nlohmann::json& output) override {
        if (input.size() < sizeof(T)) {
            std::string message =
                fmt::format("[{}.deserialize.{}] Buffer size too small", type_name<T>::name, _name);
            throw std::runtime_error(message);
        }

        spdlog::trace("[{}.deserialize.{}] Input: {:#04x}", type_name<T>::name, _name,
                      fmt::join(input, ","));
        satext::struct_detail::data_view view{input.data(), false};
        output[std::string(_name)] = satext::struct_detail::data::get<T>(view);
        spdlog::trace("[{}.deserialize.{}] Deserialized: {}", type_name<T>::name, _name,
                      output[std::string(_name)].get<T>());

        if (_next) {
            spdlog::trace("[type.deserialize.{}.{}] Calling next", type_name<T>::name, _name);
            _next->deserialize(input.subspan(sizeof(T)), output);
        }
    }

    void set_next(std::shared_ptr<serialization_base> next) { _next = next; }

private:
    std::string _name;
    std::shared_ptr<serialization_base> _next;
};

} // namespace serialization::fundamental_types
