#include "serialization/factory/factory.hpp"
#include "satext/overload.h"
#include "serialization/sat_protocol/serializer.hpp"
#include "serialization/fundamental_types/serializer.hpp"
#include "serialization/fundamental_types/type_names.hpp"
#include "serialization/telemetry_buffer/serializer.hpp"
#include <variant>
#include <span>
#include "spdlog/spdlog.h"

namespace serialization::factory {

std::shared_ptr<serialization_base>
create(std::span<sat_protocol::message_configuration> serializers);

std::shared_ptr<serialization_base>
create(fundamental_types::configuration config,
       std::span<sat_protocol::message_configuration> serializers) {
    return std::visit(
        satext::overload([serializers, config](auto type) -> std::shared_ptr<serialization_base> {
            spdlog::debug("[create_fundamental_type_serializaer] Creating fundamental type "
                          "serializer: {}, {}",
                          config.name, fundamental_types::type_name<decltype(type)>::name);
            auto result = std::make_shared<fundamental_types::type<decltype(type)>>(config.name);
            result->set_next(create(serializers));
            return result;
        }),
        config.type);
}

std::shared_ptr<serialization_base>
create(telemetry_buffer::configuration config,
       std::span<sat_protocol::message_configuration> serializers) {
    spdlog::debug("[create_telemetry_buffer_serializer] Creating telemetry buffer serializer");
    auto result = std::make_shared<telemetry_buffer::telemetry_buffer>(config.keys);
    result->set_next(create(serializers));
    return result;
}

std::shared_ptr<serialization_base>
create(std::span<sat_protocol::message_configuration> serializers) {
    if (!serializers.empty()) {
        return std::visit(
            satext::overload([serializers](auto config) -> std::shared_ptr<serialization_base> {
                return create(config, serializers.subspan(1));
            }),
            serializers.front());
    } else {
        return nullptr;
    }
}

std::shared_ptr<sat_protocol::sat_protocol> create(sat_protocol::configuration& config) {
    spdlog::debug("[create_sat_protocol] Creating sat protocol: {}", config.station_id);
    auto result = std::make_shared<sat_protocol::sat_protocol>(config.station_id);
    for (auto&& message : config.messages) {
        auto serializers = std::span(message.second);
        result->add_next(message.first, create(serializers));
    }

    return result;
}

std::tuple<std::shared_ptr<serialization_base>, std::shared_ptr<serialization_base>>
create(gcs::configuration::configuration& config) {
    std::shared_ptr<serialization_base> rx;
    std::shared_ptr<serialization_base> tx;

    rx = std::visit(satext::overload([](auto& config) -> std::shared_ptr<serialization_base> {
                        return create(config);
                    }),
                    config.rx_protocol);

    return {rx, tx};
}

} // namespace serialization::factory
