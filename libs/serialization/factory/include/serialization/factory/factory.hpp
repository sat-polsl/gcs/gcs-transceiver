#pragma once
#include "serialization/serialization_base.hpp"
#include "gcs/configuration/types.hpp"
#include <memory>

namespace serialization::factory {

std::tuple<std::shared_ptr<serialization_base>, std::shared_ptr<serialization_base>>
create(gcs::configuration::configuration& config);

}
