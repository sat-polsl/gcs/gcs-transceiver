#pragma once

namespace serialization::factory {
enum class type { sat_protocol, u8, u16, u32, i8, i16, i32, floating_number, character, boolean };
}
