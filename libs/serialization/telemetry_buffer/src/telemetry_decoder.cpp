#include <cstddef>

#include "serialization/telemetry_buffer/telemetry_decoder.hpp"
#include "satext/struct.h"
#include "serialization/telemetry_buffer/enums.hpp"
#include "spdlog/spdlog.h"
#include "fmt/format.h"
#include "satext/overload.h"
#include <variant>

using namespace satext::struct_literals;

template<>
struct fmt::formatter<serialization::telemetry_buffer::type_id> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const serialization::telemetry_buffer::type_id& t, FormatContext& ctx)
        -> decltype(ctx.out()) {
        std::string_view name = "unknown";
        switch (t) {
        case serialization::telemetry_buffer::type_id::none:
            name = "none";
            break;
        case serialization::telemetry_buffer::type_id::u8:
            name = "u8";
            break;
        case serialization::telemetry_buffer::type_id::u16:
            name = "u16";
            break;
        case serialization::telemetry_buffer::type_id::u32:
            name = "u32";
            break;
        case serialization::telemetry_buffer::type_id::i8:
            name = "i8";
            break;
        case serialization::telemetry_buffer::type_id::i16:
            name = "i16";
            break;
        case serialization::telemetry_buffer::type_id::i32:
            name = "i32";
            break;
        case serialization::telemetry_buffer::type_id::boolean:
            name = "boolean";
            break;
        case serialization::telemetry_buffer::type_id::f32:
            name = "f32";
            break;
        case serialization::telemetry_buffer::type_id::character:
            name = "character";
            break;
        case serialization::telemetry_buffer::type_id::byte:
            name = "byte";
            break;
        }
        return fmt::format_to(ctx.out(), "type_id::{}", name);
    }
};

template<>
struct fmt::formatter<serialization::telemetry_buffer::telemetry_value> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const serialization::telemetry_buffer::telemetry_value& v, FormatContext& ctx)
        -> decltype(ctx.out()) {
        return std::visit(
            satext::overload([&ctx](std::monostate) { return fmt::format_to(ctx.out(), "none"); },
                             [&ctx](auto value) { return fmt::format_to(ctx.out(), "{}", value); }),
            v);
    }
};

namespace serialization::telemetry_buffer {
telemetry_decoder::telemetry_decoder::telemetry_decoder(std::span<const std::byte> buffer) :
    buffer_{buffer} {}

template<typename T>
std::tuple<telemetry_value, std::span<const std::byte>>
get_value(std::span<const std::byte> buffer) {
    satext::struct_detail::data_view view{std::as_bytes(buffer).data(), false};
    if (buffer.size() < sizeof(T)) {
        return {std::monostate{}, buffer};
    }
    return {satext::struct_detail::data::get<T>(view), buffer.subspan(sizeof(T))};
}

std::tuple<telemetry_value, std::span<const std::byte>>
get_value(type_id type, std::span<const std::byte> buffer) {
    switch (type) {
    case type_id::none:
        return {std::monostate{}, buffer};
    case type_id::u8:
        return get_value<std::uint8_t>(buffer);
    case type_id::u16:
        return get_value<std::uint16_t>(buffer);
    case type_id::u32:
        return get_value<std::uint32_t>(buffer);
    case type_id::i8:
        return get_value<std::int8_t>(buffer);
    case type_id::i16:
        return get_value<std::int16_t>(buffer);
    case type_id::i32:
        return get_value<std::int32_t>(buffer);
    case type_id::boolean:
        return get_value<bool>(buffer);
    case type_id::f32:
        return get_value<float>(buffer);
    case type_id::character: {
        satext::struct_detail::data_view view{std::as_bytes(buffer).data(), false};
        if (buffer.size() < sizeof(std::uint8_t)) {
            return {std::monostate{}, buffer};
        }
        return {static_cast<char>(satext::struct_detail::data::get<std::uint8_t>(view)),
                buffer.subspan(sizeof(char))};
    }
    case type_id::byte: {
        satext::struct_detail::data_view view{std::as_bytes(buffer).data(), false};
        if (buffer.size() < sizeof(std::uint8_t)) {
            return {std::monostate{}, buffer};
        }
        return {static_cast<std::byte>(satext::struct_detail::data::get<std::uint8_t>(view)),
                buffer.subspan(sizeof(std::byte))};
    }
    }
    return {std::monostate{}, buffer};
}

satext::expected<key_value, std::monostate> telemetry_decoder::decode() {
    return satext::unpack("<H"_fmt, buffer_)
        .and_then([this](auto unpacked) -> satext::expected<key_value, std::monostate> {
            buffer_ = buffer_.subspan(key_size);
            auto [key] = unpacked;
            spdlog::trace("[telemetry_decoder] Key: {:#06x}", key);
            std::uint16_t index = key >> index_shift;
            auto value_type = type_id(key & type_mask);
            spdlog::trace("[telemetry_decoder] Index: {}", index);
            spdlog::trace("[telemetry_decoder] Type: {}", value_type);

            auto [value, buffer] = get_value(value_type, buffer_);
            buffer_ = buffer;

            spdlog::trace("[telemetry_decoder] Value: {}", value);
            return key_value{index, value};
        });
}

bool telemetry_decoder::is_empty() const { return buffer_.empty(); }

} // namespace serialization::telemetry_buffer
