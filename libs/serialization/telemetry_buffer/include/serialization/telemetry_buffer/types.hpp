#pragma once
#include <cstdint>
#include <variant>

namespace serialization::telemetry_buffer {

using telemetry_value =
    std::variant<std::monostate, std::uint8_t, std::uint16_t, std::uint32_t, std::int8_t,
                 std::int16_t, std::int32_t, bool, float, char, std::byte>;

struct key_value {
    std::uint16_t id;
    telemetry_value value;
};

} // namespace serialization::telemetry_buffer
