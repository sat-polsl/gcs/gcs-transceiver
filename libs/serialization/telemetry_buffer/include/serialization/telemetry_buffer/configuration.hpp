#pragma once
#include <string>
#include <cstdint>
#include <vector>

namespace serialization::telemetry_buffer {

struct key {
    std::uint32_t id;
    std::string name;
};

struct configuration {
    std::vector<key> keys;
};
} // namespace serialization::telemetry_buffer
