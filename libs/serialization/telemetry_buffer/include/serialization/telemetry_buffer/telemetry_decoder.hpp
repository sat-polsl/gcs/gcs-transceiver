#pragma once
#include <cstdint>
#include <span>
#include "satext/expected.h"
#include "serialization/telemetry_buffer/types.hpp"

namespace serialization::telemetry_buffer {
class telemetry_decoder {
public:
    explicit telemetry_decoder(std::span<const std::byte> buffer);
    satext::expected<key_value, std::monostate> decode();
    [[nodiscard]] bool is_empty() const;

private:
    std::span<const std::byte> buffer_;

    static constexpr auto index_shift = 4u;
    static constexpr auto type_mask = 0x0f;
    static constexpr auto key_size = sizeof(std::uint16_t);
};

} // namespace serialization::telemetry_buffer
