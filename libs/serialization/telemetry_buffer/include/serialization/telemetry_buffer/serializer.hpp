#pragma once
#include <concepts>
#include "serialization/serialization_base.hpp"
#include <stdexcept>
#include <memory>
#include "spdlog/spdlog.h"
#include "serialization/telemetry_buffer/configuration.hpp"
#include "serialization/telemetry_buffer/telemetry_decoder.hpp"
#include <ranges>
#include "satext/overload.h"

namespace serialization::telemetry_buffer {

class telemetry_buffer : public serialization_base {
public:
    explicit telemetry_buffer(std::vector<key> keys) : _keys{std::move(keys)} {}

    void serialize(const nlohmann::json&, std::span<std::byte>) override {}

    void deserialize(std::span<const std::byte> input, nlohmann::json& output) override {
        telemetry_decoder decoder(input);
        while (!decoder.is_empty()) {
            auto decode_result = decoder.decode();
            if (!decode_result.has_value()) {
                break;
            }

            auto value = decode_result.value();
            if (auto it = std::ranges::find(_keys, value.id, &key::id); it != _keys.end()) {
                std::visit(satext::overload([](std::monostate) {},
                                            [&output, it](auto v) {
                                                spdlog::info(
                                                    "[telemetry_buffer] Assigning {} to {} field",
                                                    v, it->name);
                                                output[it->name] = v;
                                            }),
                           value.value);
            } else {
                spdlog::error("[telemetry_buffer] {} ID not found", value.id);
            }
        }

        if (_next) {
            _next->deserialize(input, output);
        }
    }

    void set_next(std::shared_ptr<serialization_base> next) { _next = next; }

private:
    std::vector<key> _keys;
    std::shared_ptr<serialization_base> _next;
};

} // namespace serialization::telemetry_buffer
