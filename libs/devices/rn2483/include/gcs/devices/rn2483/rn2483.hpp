#pragma once
#include "gcs/serial/serial.hpp"
#include <boost/asio.hpp>
#include <array>
#include "gcs/devices/rn2483/enums.hpp"
#include "satext/noncopyable.h"
#include "satext/type_traits.h"
#include "fmt/format.h"
#include "spdlog/spdlog.h"
#include <vector>
#include <chrono>
#include "gcs/devices/rn2483/configuration.hpp"
#include "gcs/devices/rn2483/formatters.hpp"
#include "gcs/utility/invoke.hpp"
#include <memory>
#include <boost/date_time/posix_time/posix_time_duration.hpp>
#include <boost/algorithm/string.hpp>
#include "satext/charconv.h"

namespace gcs::devices::rn2483 {

class rn2483 : private satext::noncopyable {
public:
    template<typename Handler>
    class response_handler {
    public:
        template<typename HandlerType>
        response_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, std::size_t size) {
            if (!error) {
                auto received = std::string_view{_rn2483._remaining_buffer.data(), size};
                _rn2483._remaining_buffer = _rn2483._remaining_buffer.subspan(size);
                spdlog::debug("[Devices.rn2483.response_handler] Received '{}'", received);

                if (received.find_first_of(lf) == std::string_view::npos) {
                    _rn2483._serial.async_read(
                        std::as_writable_bytes(_rn2483._remaining_buffer),
                        response_handler<Handler>(_rn2483, std::move(_handler)));
                } else {
                    auto response_size = _rn2483._buffer.size() - _rn2483._remaining_buffer.size();
                    auto response = std::string{_rn2483._buffer.data(), response_size};
                    std::erase_if(response,
                                  [](char c) { return (c == '\r') || (c == '\n') || (c == 0); });
                    utility::invoke(_handler, error, map_status(response));
                }
            } else {
                spdlog::error("[Devices.rn2483.response_handler] Error: {}", error.message());
                utility::invoke(_handler, error, status::ok);
            }
        }

    private:
        rn2483& _rn2483;
        Handler _handler;
    };

    template<typename Handler>
    class mac_pause_response_handler {
    public:
        template<typename HandlerType>
        mac_pause_response_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, std::size_t size) {
            if (!error) {
                auto received = std::string_view{_rn2483._remaining_buffer.data(), size};
                _rn2483._remaining_buffer = _rn2483._remaining_buffer.subspan(size);
                spdlog::debug("[Devices.rn2483.mac_pause_response_handler] Received {}", received);

                if (received.find_first_of(lf) == std::string_view::npos) {
                    _rn2483._serial.async_read(
                        std::as_writable_bytes(_rn2483._remaining_buffer),
                        mac_pause_response_handler<Handler>(_rn2483, std::move(_handler)));
                } else {
                    utility::invoke(_handler, error, status::ok);
                }
            } else {
                spdlog::error("[Devices.rn2483.mac_pause_response_handler] Error {}",
                              error.message());
                utility::invoke(_handler, error, status::ok);
            }
        }

    private:
        rn2483& _rn2483;
        Handler _handler;
    };

    template<typename Handler>
    class request_handler {
    public:
        template<typename HandlerType>
        request_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, std::size_t) {
            spdlog::debug("[Devices.rn2483.request_handler] error: {}", error.message());
            if (!error) {
                std::ranges::fill(_rn2483._buffer, 0);
                _rn2483._remaining_buffer = _rn2483._buffer;
                _rn2483._serial.async_read(std::as_writable_bytes(_rn2483._remaining_buffer),
                                           response_handler<Handler>(_rn2483, std::move(_handler)));
            } else {
                utility::invoke(_handler, error, status::ok);
            }
        }

    private:
        rn2483& _rn2483;
        Handler _handler;
    };

    template<typename Handler>
    class map_pause_request_handler {
    public:
        template<typename HandlerType>
        map_pause_request_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, std::size_t) {
            spdlog::debug("[Devices.rn2483.mac_pause_request_hadnler] error: {}", error.message());
            if (!error) {
                std::ranges::fill(_rn2483._buffer, 0);
                _rn2483._remaining_buffer = _rn2483._buffer;
                _rn2483._serial.async_read(
                    std::as_writable_bytes(_rn2483._remaining_buffer),
                    mac_pause_response_handler<Handler>(_rn2483, std::move(_handler)));
            } else {
                utility::invoke(_handler, error, status::ok);
            }
        }

    private:
        rn2483& _rn2483;
        Handler _handler;
    };

    template<typename Handler>
    class lora_configuration_handler
        : public std::enable_shared_from_this<lora_configuration_handler<Handler>> {
        enum class state {
            modulation,
            frequency,
            power,
            spreading_factor,
            bandwidth,
            coding_rate,
            sync,
            iq_inversion,
            watchdog,
            finished
        };

    public:
        template<typename HandlerType>
        lora_configuration_handler(rn2483& rn2483,
                                   const gcs::devices::rn2483::configuration& configuration,
                                   HandlerType&& handler) :
            _rn2483{rn2483},
            _configuration{configuration},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, status radio_status) {
            if (error || radio_status != status::ok) {
                spdlog::error("[Devices.rn2483.lora_configuration_handler] Error: {}, Status: {}",
                              error.message(), radio_status);
                utility::invoke(_handler, error, configuration_status::error);
            }
            spdlog::trace("[Devices.rn2483.lora_configuration_handler] State: {}",
                          satext::to_underlying_type(_state));

            switch (_state) {
            case state::modulation:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting modulation: {}",
                              _configuration.modulation);
                _rn2483.set_modulation(_configuration.modulation, this->shared_from_this());
                _state = state::frequency;
                break;
            case state::frequency:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting frequency: {}",
                              _configuration.frequency);
                _rn2483.set_frequency(_configuration.frequency, this->shared_from_this());
                _state = state::power;
                break;
            case state::power:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting power: {}",
                              _configuration.power);
                _rn2483.set_power(_configuration.power, this->shared_from_this());
                _state = state::spreading_factor;
                break;
            case state::spreading_factor:
                spdlog::debug(
                    "[Devices.rn2483.lora_configuration_handler] Setting spreading factor: {}",
                    _configuration.spreading_factor);
                _rn2483.set_spreading_factor(_configuration.spreading_factor,
                                             this->shared_from_this());
                _state = state::bandwidth;
                break;
            case state::bandwidth:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting bandwidth: {}",
                              satext::to_underlying_type(_configuration.lora_bandwidth));
                _rn2483.set_bandwidth(_configuration.lora_bandwidth, this->shared_from_this());
                _state = state::coding_rate;
                break;
            case state::coding_rate:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting coding rate: {}",
                              _configuration.coding_rate);
                _rn2483.set_coding_rate(_configuration.coding_rate, this->shared_from_this());
                _state = state::sync;
                break;
            case state::sync:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting sync");
                _rn2483.set_sync(_configuration.sync, this->shared_from_this());
                _state = state::iq_inversion;
                break;
            case state::iq_inversion:
                spdlog::debug("[Devices.rn2483.lora_configuration_handler] Setting iq inversion {}",
                              _configuration.iq_inversion);
                _rn2483.set_iq_inversion(_configuration.iq_inversion, this->shared_from_this());
                _state = state::watchdog;
                break;
            case state::watchdog:
                spdlog::debug(
                    "[Devices.rn2483.lora_configuration_handler] Setting watchdog timeout {} ms",
                    _configuration.watchdog_timeout.count());
                _rn2483.set_watchdog_timeout(_configuration.watchdog_timeout,
                                             this->shared_from_this());
                _state = state::finished;
                break;
            case state::finished:
                utility::invoke(_handler, error, configuration_status::done);
            }
        }

    private:
        rn2483& _rn2483;
        const gcs::devices::rn2483::configuration& _configuration;
        Handler _handler;
        state _state{state::modulation};
    };

    template<typename Handler>
    class radio_rx_handler {
    public:
        template<typename HandlerType>
        radio_rx_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, status radio_status) {
            if (!error) {
                spdlog::debug("[Devices.rn2483.radio_rx_handler] Status: {}", radio_status);
                if (radio_status == status::ok) {
                    _rn2483._serial.async_read(
                        std::as_writable_bytes(std::span(_rn2483._buffer)),
                        radio_response_handler<Handler>(_rn2483, std::move(_handler)));
                }
            } else {
                spdlog::error("[Devices.rn2483.radio_rx_handler] Error {}", error.message());
                utility::invoke(_handler, error, status::ok, std::vector<std::byte>{});
            }
        }

    private:
        rn2483& _rn2483;
        Handler _handler;
    };

    template<typename Handler>
    class radio_response_handler {
    public:
        template<typename HandlerType>
        radio_response_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, std::size_t size) {
            if (error) {
                spdlog::error("[Devices.rn2483.parse_radio_response] Error {}", error.message());
                utility::invoke(_handler, error, status::ok, std::vector<std::byte>{});
            } else {
                auto received = std::string_view{_rn2483._remaining_buffer.data(), size};
                _rn2483._remaining_buffer = _rn2483._remaining_buffer.subspan(size);
                spdlog::debug("[Devices.rn2483.parse_radio_response] Received '{}'", received);

                if (received.find_first_of(lf) == std::string_view::npos) {
                    _rn2483._serial.async_read(
                        std::as_writable_bytes(_rn2483._remaining_buffer),
                        radio_response_handler<Handler>(_rn2483, std::move(_handler)));
                } else {
                    auto response_size = _rn2483._buffer.size() - _rn2483._remaining_buffer.size();
                    auto response = std::string(_rn2483._buffer.data(), response_size);
                    spdlog::debug("[Devices.rn2483.parse_radio_response] Response: '{}'", response);
                    auto [radio_status, data] = tokenize(response);
                    spdlog::debug("[Devices.rn2483.parse_radio_response] Radio status: {}",
                                  radio_status);
                    std::vector<std::byte> result{};
                    if (radio_status == status::radio_rx) {
                        result = unhexify(data);
                    }
                    utility::invoke(_handler, error, radio_status, result);
                }
            }
        }

    private:
        std::vector<std::byte> unhexify(std::string_view data) {
            spdlog::debug("[Devices.rn2483.unhexify] Data: '{}', size: {}", data, data.size());
            if (data.empty() || data.size() % 2 != 0) {
                spdlog::trace("[Devices.rn2483.unhexify] Wrong data.");
                return {};
            }

            std::vector<std::byte> result{};
            while (!data.empty()) {
                std::uint8_t value;

                auto token = data.substr(0, 2);
                spdlog::trace("[Devices.rn2483.unhexify] Next token: {}, hex: '{:#04x}', size: {}",
                              token, fmt::join(token, ", "), token.size());

                if (auto [_, e] = satext::from_chars(token, value, 16); e != std::errc{}) {
                    spdlog::trace("[Devices.rn2483.unhexify] Error while processing: {}", token);
                    return result;
                }

                data = data.substr(2);
                spdlog::trace("[Devices.rn2483.unhexify] Processed: {:#04x}", value);
                spdlog::trace("[Devices.rn2483.unhexify] Remaining: {}", data);
                result.push_back(std::byte{value});
            }

            return result;
        }

        std::tuple<status, std::string> tokenize(std::string line) {
            status radio_status{};
            std::string result{};

            std::erase_if(line, [](char c) { return (c == '\r') || (c == '\n') || (c == 0); });

            std::vector<std::string> tokens;
            boost::split(
                tokens, line, [](char c) { return c == ' '; }, boost::token_compress_on);
            if (!tokens.empty()) {
                radio_status = map_status(tokens[0]);
            }
            if (radio_status == status::radio_rx) {
                if (tokens.size() >= 2) {
                    result = tokens[1];
                }
            }
            return {radio_status, result};
        }

        rn2483& _rn2483;
        Handler _handler;
    };

    template<typename Handler>
    class receive_handler {
    public:
        template<typename HandlerType>
        receive_handler(rn2483& rn2483, HandlerType&& handler) :
            _rn2483{rn2483},
            _handler{std::forward<HandlerType>(handler)} {}

        void operator()(boost::system::error_code error, status radio_status) {
            if (error || radio_status != status::ok) {
                spdlog::error("[Devices.rn2483.receive_handler] Error: {}, Status: {}",
                              error.message(), radio_status);
                utility::invoke(_handler, error, radio_status, std::vector<std::byte>{});
            } else {
                _rn2483.request_response(radio_rx_handler<Handler>(_rn2483, std::move(_handler)),
                                         command::radio, command::rx, 0);
            }
        }

    private:
        rn2483& _rn2483;
        Handler _handler;
    };

    rn2483(gcs::serial::serial& serial) : _serial{serial} {}

    template<typename Handler>
    void mac_pause(Handler&& handler) {
        auto end = fmt::format_to(_buffer.begin(), "{} {}{}", command::mac, command::pause, crlf);

        send_command({_buffer.begin(), end},
                     map_pause_request_handler<Handler>(*this, std::forward<Handler>(handler)));
    }

    template<typename Handler>
    void set_modulation(std::string_view modulation, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::modulation, modulation);
    }

    template<typename Handler>
    void set_frequency(std::uint32_t frequency, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::frequency, frequency);
    }

    template<typename Handler>
    void set_power(std::int32_t power, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::power, power);
    }

    template<typename Handler>
    void set_spreading_factor(std::string_view spreadign_factor, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::spreading_factor, spreadign_factor);
    }

    template<typename Handler>
    void set_bandwidth(bandwidth bw, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::bandwidth, satext::to_underlying_type(bw));
    }

    template<typename Handler>
    void set_coding_rate(std::string_view coding_rate, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::coding_rate, coding_rate);
    }

    template<typename Handler>
    void set_sync(std::vector<std::byte> sync, Handler&& handler) {
        decltype(_buffer.begin()) it{};
        it = fmt::format_to(_buffer.begin(), "{} {} {} ", command::radio, command::set,
                            command::sync);
        it = hexify(sync, std::span(it, sync.size() * 2)).base();
        it = fmt::format_to(it, "{}", crlf);

        send_command({_buffer.begin(), it},
                     request_handler<Handler>(*this, std::forward<Handler>(handler)));
    }

    template<typename Handler>
    void set_iq_inversion(std::string_view iqi, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::iq_inversion, iqi);
    }

    template<typename Handler>
    void set_watchdog_timeout(std::chrono::milliseconds timeout, Handler&& handler) {
        return request_response(std::forward<Handler>(handler), command::radio, command::set,
                                command::watchdog, timeout.count());
    }

    template<typename Handler>
    void configure(const gcs::devices::rn2483::configuration& cfg, Handler&& handler) {
        mac_pause(std::make_shared<lora_configuration_handler<Handler>>(
            *this, cfg, std::forward<Handler>(handler)));
    }

    template<typename Handler>
    void receive(std::chrono::milliseconds timeout, Handler&& handler) {
        set_watchdog_timeout(timeout,
                             receive_handler<Handler>(*this, std::forward<Handler>(handler)));
    }

private:
    template<typename Handler>
    void send_command(std::string_view command, Handler&& handler) {
        spdlog::debug("[Devices.rn2483] Sending: '{}'", command);
        _serial.async_write(std::as_bytes(std::span(command.data(), command.size())),
                            std::forward<Handler>(handler));
    }

    template<typename Handler, typename... Args>
    void request_response(Handler&& handler, Args&&... args) {
        decltype(_buffer.begin()) end{};
        if constexpr (sizeof...(Args) == 4) {
            end = fmt::format_to(_buffer.begin(), "{} {} {} {}{}", args..., crlf);
        } else if constexpr (sizeof...(Args) == 3) {
            end = fmt::format_to(_buffer.begin(), "{} {} {}{}", args..., crlf);
        }

        send_command({_buffer.begin(), end},
                     request_handler<Handler>(*this, std::forward<Handler>(handler)));
    }

    static auto hexify(std::span<const std::byte> input, std::span<char> output) {
        auto it = output.begin();
        if ((output.size() % 2 == 1) || (output.size() / 2 < input.size())) {
            return it;
        }

        for (auto i : input) {
            it = fmt::format_to(it, "{:02x}", satext::to_underlying_type(i));
        }
        return it;
    }

    static status map_status(std::string_view response) {
        if (response == response::ok) {
            return status::ok;
        } else if (response == response::version) {
            return status::version;
        } else if (response == response::busy) {
            return status::busy;
        } else if (response == response::radio_err) {
            return status::radio_error;
        } else if (response == response::radio_tx_ok) {
            return status::radio_tx_ok;
        } else if (response == response::invalid_parameter) {
            return status::invalid_parameter;
        } else if (response == response::radio_rx) {
            return status::radio_rx;
        } else {
            return status::parse_error;
        }
    }

    gcs::serial::serial& _serial;
    std::array<char, 4098> _buffer{};
    std::span<char> _remaining_buffer{};

    static constexpr auto crlf = std::string_view{"\r\n"};
    static constexpr auto lf = std::string_view{"\n"};
};

} // namespace gcs::devices::rn2483
