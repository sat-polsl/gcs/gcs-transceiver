#pragma once
#include "fmt/format.h"
#include "gcs/devices/rn2483/enums.hpp"

template<>
struct fmt::formatter<gcs::devices::rn2483::status> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const gcs::devices::rn2483::status& s, FormatContext& ctx) -> decltype(ctx.out()) {
        std::string_view name = "unknown";
        switch (s) {
        case gcs::devices::rn2483::status::ok:
            name = "ok";
            break;
        case gcs::devices::rn2483::status::invalid_parameter:
            name = "invalid parameter";
            break;
        case gcs::devices::rn2483::status::busy:
            name = "busy";
            break;
        case gcs::devices::rn2483::status::radio_rx:
            name = "radio rx";
            break;
        case gcs::devices::rn2483::status::radio_tx_ok:
            name = "radio tx ok";
            break;
        case gcs::devices::rn2483::status::radio_error:
            name = "radio error";
            break;
        case gcs::devices::rn2483::status::version:
            name = gcs::devices::rn2483::response::version;
            break;
        case gcs::devices::rn2483::status::parse_error:
            name = "parse error";
            break;
        }
        return fmt::format_to(ctx.out(), "{}", name);
    }
};
