#pragma once
#include <string_view>
#include "gcs/devices/rn2483/enums.hpp"
#include <chrono>
#include <vector>

namespace gcs::devices::rn2483 {

/**
 * @ingroup radio
 * @{
 */

/**
 * @brief RN2483 radio configuration struct.
 */
struct configuration {
    /**
     * @brief Modulation. "lora" or "fsk"
     */
    std::string_view modulation{modulation::lora};

    /**
     * @brief Frequency from 433000000 to 434800000 Hz.
     */
    std::uint32_t frequency{};

    /**
     * @brief Power in dBm from -3 to 15.
     */
    std::int32_t power{15};

    /**
     * @brief LoRa spreading factor.
     */
    std::string_view spreading_factor{spreading_factor::sf12};

    /**
     * @brief LoRa bandwidth.
     */
    bandwidth lora_bandwidth{bandwidth::bw125khz};

    /**
     * @brief LoRa coding rate.
     */
    std::string_view coding_rate{coding_rate::cr4_5};

    /**
     * @brief sync word. 1-byte for LoRa, 8-bytes for FSK.
     */
    std::vector<std::byte> sync{std::byte{0x12}};

    /**
     * @brief LoRa IQ inversion
     */
    std::string_view iq_inversion{on_off::off};

    /**
     * @brief Watchdog timeout in milliseconds. Zero means watchdog is disabled.
     */
    std::chrono::milliseconds watchdog_timeout{0};
};

/** @} */

} // namespace gcs::devices::rn2483
