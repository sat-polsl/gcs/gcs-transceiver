#!/bin/sh
exec gcs-transceiver --proxy ${PROXY_IP} --pub ${PUB_PORT} --sub ${SUB_PORT} --config ${CONFIG_FILE} ${LOG_ARGS}
