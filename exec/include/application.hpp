#pragma once
#include <cstdint>
#include <mutex>
#include <boost/asio.hpp>
#include "azmq/socket.hpp"
#include "gcs/transceivers/transceiver_base.hpp"
#include <memory>
#include <vector>

namespace application {
class application {
public:
    application() = default;
    application(const application&) = delete;
    application& operator=(const application&) = delete;
    application(application&&) noexcept = delete;
    application& operator=(application&&) noexcept = delete;
    ~application() = default;

    std::int32_t run(int argc, char** argv);

private:
    bool parse_args(int argc, char** argv);

    boost::asio::io_context _ctx;

    azmq::pub_socket _pub_socket{_ctx};

    std::vector<std::shared_ptr<gcs::transceivers::transceiver_base>> _transceivers;

    std::string _address;
    std::string _pub_port;
    std::string _sub_port;
    std::string _config_file;
    bool _debug;
    bool _trace;
};

} // namespace application
