#include "application.hpp"
#include "spdlog/spdlog.h"
#include "cxxopts.hpp"
#include "serialization/factory/factory.hpp"
#include "gcs/configuration/read_configuration.hpp"
#include "gcs/transceivers/factory.hpp"

namespace application {

std::int32_t application::run(int argc, char** argv) {
    if (!parse_args(argc, argv)) {
        return -1;
    }

    spdlog::info("App start!");
    if (_debug) {
        spdlog::set_level(spdlog::level::debug);
        spdlog::info("Log level: debug");
    }
    if (_trace) {
        spdlog::set_level(spdlog::level::trace);
        spdlog::info("Log level: trace");
    }

    spdlog::info("Connecting publisher socket to {}:{}", _address, _pub_port);
    _pub_socket.set_option(azmq::socket::linger(0));
    _pub_socket.connect("tcp://" + _address + ":" + _pub_port);

    auto configuration = gcs::configuration::read_configuration(_config_file);

    auto [rx, tx] = serialization::factory::create(configuration);

    gcs::transceivers::factory::create(configuration, _ctx, _pub_socket, rx, tx, _transceivers);

    for (auto&& transceiver : _transceivers) {
        transceiver->start();
    }

    _ctx.run();
    return 0;
}

bool application::parse_args(int argc, char** argv) {
    cxxopts::Options opts("gcs-transceiver", "GCS transceiver");

    // clang-format off
    opts.add_options()
        ("h,help", "Print usage")
        ("proxy", "Proxy adress", cxxopts::value<std::string>())
        ("pub", "Publisher port", cxxopts::value<std::string>())
        ("sub", "Subscriber port", cxxopts::value<std::string>())
        ("config", "Config file", cxxopts::value<std::string>())
        ("debug", "Debug log level")
        ("trace", "Trace log level")
        ;
    // clang-format on

    auto result = opts.parse(argc, argv);

    if (result.count("help")) {
        std::printf("%s\n", opts.help().c_str());
        return false;
    }

    try {
        _address = result["proxy"].as<std::string>();
        _pub_port = result["pub"].as<std::string>();
        _sub_port = result["sub"].as<std::string>();
        _config_file = result["config"].as<std::string>();
        _debug = result.count("debug");
        _trace = result.count("trace");
    } catch (const std::exception&) {
        std::printf("%s\n", opts.help().c_str());
        return false;
    }

    return true;
}

} // namespace application
