# GCS Transceiver

## Building
This application is only **Linux compatible** (on Windows use [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) to build)

### Dependencies
- gcc 11 or higher
- pkgconfig
- libgnutls30
- libsodium-dev
- libboost-dev

### Configuration
Environment variables configuration:

| Variable    | Description                                          |
|-------------|------------------------------------------------------|
| PROXY_IP    | ZMQ proxy address                                    |
| PUB_PORT    | Port for **publisher** sockets                       |
| SUB_PORT    | Port for **subscriber** sockets                      |
| LOG_ARGS    | `--debug` or `--trace` to enable debug or trace logs |
| CONFIG_FILE | Path to config file                                  |
