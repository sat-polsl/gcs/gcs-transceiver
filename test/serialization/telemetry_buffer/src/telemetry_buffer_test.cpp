#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "serialization/telemetry_buffer/serializer.hpp"
#include <array>
#include "nlohmann/json.hpp"

namespace {
using namespace serialization::telemetry_buffer;
using namespace testing;

TEST(TelemetryBufferSerializer, EmptyBuffer) {
    std::array<std::byte, 1> buffer = {};
    auto serializer = telemetry_buffer({});
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.empty(), Eq(true));
}

TEST(TelemetryBufferSerializer, SingleU32) {
    std::array<std::byte, 6> buffer = {std::byte{0x03}, std::byte{0x00}, std::byte{0xde},
                                       std::byte{0xc0}, std::byte{0xad}, std::byte{0xde}};
    auto serializer = telemetry_buffer({{0, "test"}});
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::uint32_t>(), Eq(std::uint32_t{0xdeadc0de}));
}

TEST(TelemetryBufferSerializer, SingleFloat) {
    std::array<std::byte, 6> buffer = {std::byte{0x18}, std::byte{0x00}, std::byte{0x00},
                                       std::byte{0x00}, std::byte{0x4d}, std::byte{0x42}};
    auto serializer = telemetry_buffer({{1, "test"}});
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<float>(), FloatEq(51.25f));
}

TEST(TelemetryBufferSerializer, U32AndFloat) {
    std::array<std::byte, 12> buffer = {std::byte{0x03}, std::byte{0x00}, std::byte{0xde},
                                        std::byte{0xc0}, std::byte{0xad}, std::byte{0xde},
                                        std::byte{0x18}, std::byte{0x00}, std::byte{0x00},
                                        std::byte{0x00}, std::byte{0x4d}, std::byte{0x42}};
    auto serializer = telemetry_buffer({{0, "u32"}, {1, "float"}});
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("u32"), Eq(true));
    ASSERT_THAT(result["u32"].get<std::uint32_t>(), Eq(std::uint32_t{0xdeadc0de}));
    ASSERT_THAT(result.contains("float"), Eq(true));
    ASSERT_THAT(result["float"].get<float>(), FloatEq(51.25f));
}

TEST(TelemetryBufferSerializer, U32AndWrongId) {
    std::array<std::byte, 12> buffer = {std::byte{0x03}, std::byte{0x00}, std::byte{0xde},
                                        std::byte{0xc0}, std::byte{0xad}, std::byte{0xde},
                                        std::byte{0x28}, std::byte{0x00}, std::byte{0x00},
                                        std::byte{0x00}, std::byte{0x4d}, std::byte{0x42}};
    auto serializer = telemetry_buffer({{0, "u32"}, {1, "float"}});
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("u32"), Eq(true));
    ASSERT_THAT(result["u32"].get<std::uint32_t>(), Eq(std::uint32_t{0xdeadc0de}));
    ASSERT_THAT(result.contains("float"), Eq(false));
}

} // namespace
