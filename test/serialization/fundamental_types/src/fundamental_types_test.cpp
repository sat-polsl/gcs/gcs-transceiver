#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "serialization/fundamental_types/serializer.hpp"
#include <array>
#include "nlohmann/json.hpp"

namespace {
using namespace serialization::fundamental_types;
using namespace testing;

TEST(TypeSerializer, DeserializeFailure) {
    std::array<std::byte, 1> buffer = {};

    auto serializer = type<std::uint8_t>("test");
    nlohmann::json result{};
    EXPECT_THROW(serializer.deserialize(std::span(buffer.data(), 0), result), std::runtime_error);
}

TEST(TypeSerializer, DeserializeU8Success) {
    std::array<std::byte, 1> buffer = {std::byte{0xaa}};

    auto serializer = type<std::uint8_t>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::uint8_t>(), Eq(0xaa));
}

TEST(TypeSerializer, DeserializeU16Success) {
    std::array<std::byte, 2> buffer = {std::byte{0xad}, std::byte{0xde}};

    auto serializer = type<std::uint16_t>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::uint16_t>(), Eq(std::uint16_t{0xdead}));
}

TEST(TypeSerializer, DeserializeU32) {
    std::array<std::byte, 4> buffer = {std::byte{0xde}, std::byte{0xc0}, std::byte{0xad},
                                       std::byte{0xde}};

    auto serializer = type<std::uint32_t>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::uint32_t>(), Eq(std::uint32_t{0xdeadc0de}));
}

TEST(TypeSerializer, DeserializeI8Success) {
    std::array<std::byte, 1> buffer = {std::byte{0xaa}};

    auto serializer = type<std::int8_t>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::int8_t>(), Eq(std::int8_t(0xaa)));
}

TEST(TypeSerializer, DeserializeI16Success) {
    std::array<std::byte, 2> buffer = {std::byte{0xad}, std::byte{0xde}};

    auto serializer = type<std::int16_t>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::int16_t>(), Eq(std::int16_t(0xdead)));
}

TEST(TypeSerializer, DeserializeI32) {
    std::array<std::byte, 4> buffer = {std::byte{0xde}, std::byte{0xc0}, std::byte{0xad},
                                       std::byte{0xde}};

    auto serializer = type<std::int32_t>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<std::int32_t>(), Eq(std::int32_t(0xdeadc0de)));
}

TEST(TypeSerializer, DeserializeFloatSuccess) {
    std::array<std::byte, 4> buffer = {std::byte{0x00}, std::byte{0x00}, std::byte{0x20},
                                       std::byte{0x41}};

    auto serializer = type<float>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<float>(), Eq(10.0f));
}

TEST(TypeSerializer, DeserializeCharSuccess) {
    std::array<std::byte, 1> buffer = {std::byte{0x41}};

    auto serializer = type<char>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<char>(), Eq('A'));
}

TEST(TypeSerializer, DeserializeBoolSuccess) {
    std::array<std::byte, 1> buffer = {std::byte{0x01}};

    auto serializer = type<bool>("test");
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("test"), Eq(true));
    ASSERT_THAT(result["test"].get<bool>(), Eq(true));
}

} // namespace
