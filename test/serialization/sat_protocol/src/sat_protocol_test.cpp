#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <array>
#include "nlohmann/json.hpp"
#include "serialization/sat_protocol/serializer.hpp"

namespace {
using namespace serialization::sat_protocol;
using namespace testing;

TEST(SatProtocol, DeserializeSuccess) {
    std::array<std::byte, 10> buffer = {
        std::byte{0x53}, std::byte{0x50}, std::byte{0x39}, std::byte{0x46}, std::byte{0x54},
        std::byte{0x4c}, std::byte{0x10}, std::byte{0x20}, std::byte{0x30}, std::byte{0x40}};

    sat_protocol serializer{10};
    nlohmann::json result{};
    serializer.deserialize(buffer, result);

    ASSERT_THAT(result.contains("callsign"), Eq(true));
    ASSERT_THAT(result.contains("spacecraft_id"), Eq(true));
    ASSERT_THAT(result.contains("tx_counter"), Eq(true));
    ASSERT_THAT(result.contains("message_id"), Eq(true));
    ASSERT_THAT(result.contains("message_size"), Eq(true));
    ASSERT_THAT(result.contains("station_id"), Eq(true));
    ASSERT_THAT(result["callsign"].get<std::string>(), Eq("SP9FTL"));
    ASSERT_THAT(result["spacecraft_id"].get<std::uint8_t>(), Eq(16));
    ASSERT_THAT(result["tx_counter"].get<std::uint8_t>(), Eq(32));
    ASSERT_THAT(result["message_id"].get<std::uint8_t>(), Eq(48));
    ASSERT_THAT(result["message_size"].get<std::uint8_t>(), Eq(64));
    ASSERT_THAT(result["station_id"].get<std::uint8_t>(), Eq(10));
}

TEST(SatProtocol, DeserializeFailure) {
    std::array<std::byte, 5> buffer{};

    sat_protocol serializer{10};
    nlohmann::json result{};
    EXPECT_THROW(serializer.deserialize(buffer, result), std::runtime_error);
}

} // namespace
